# YHTEYSTTIEDOT
##### Nimi:		    Jere Rahikainen
##### Puhelinnumero:	0405269339
##### Sähköposti:		j.rahilainen@gmail.com
##### Osoite:		    Humalistonkatu 5 A 11, 00250 Helsinki
##### LinkedIn:		https://www.linkedin.com/in/jere-rahikainen-4947b8126/

# TAVOITE
Tavoitteena on löytää työpaikka, jossa saan toteuttaa itseäni ohjelmoinnin ammattilaisena sekä menestyä yhdessä yrityksen kanssa.

# KOULUTUS
### Suomen Liikemiesten Kauppaopisto, Mäkelänrinteen Lukio  2010, 2011
-	Yo-merkonomi, asiakaspalvelu ja markkinointi
### Metropolian ammattikorkeakoulu  2020
-	Insinööri, tieto- ja viestintätekniikka, ohjelmistotuotanto

# KOKEMUS
### Enermix Oy  14.5.2018 – 1.6.2020
-	Ohjelmistosuunnittelija
-	Android-sovellus (kesätyö 2018)
-	Automaatiotestaus, insinöörityö
### K-Market Kielotie  10.11.2008 – 31.08.2017
-	Kaupan töitä opiskelun ohella (30.8.2016 – 31.8.2017)
-	Teollisten sekä einesten osastonhoitaja (1.1.2014 – 30.8.2016)
-	Kaupan töitä (10.11.2008 – 1.1.2014)
### Erilaisia pätkätöitä  10.11.2006 – 10.11.2013
-	Peltikattojen asentaja
-	Osaston sihteeri, HUS
-	Puhelinmyyjä
-	Puusepän verstaalla apulaisena
-	Messurakentaja

# OSAAMISALUEET
-	Hallitsen asiakaspalvelun
-	Olen erinomainen ryhmätyöskentelijä
-	JAVA
-	Python
-	HTML/CSS
-	JavaScript
-	JSON 
-	AJAX 
-	REST
-	React.js
-	vue.js 
-	Express 
-	Node.js
-	sequelize
-	passport
-	PHP
-	E2E automaatiotestaus - Robot Framework & SeleniumLibrary
-	Funktionaalinen ohjelmointi - JAVA & JavaScript
-	C/C++ - Olio-ohjelmointia hyödyntäen ohjelmien rakentaminen
-	UNIX/LINUX - Systeemikutsut siirrännässä ja tiedostojenhallinnassa
sekä samanaikaisten prosessien ja säikeiden välisessä kommunikoinnissa, 
synkronoinnissa ja kilpailutilanteiden hallinnassa.
-	React-Native - perusteet
-	MySQL/NoSQL – suunnittelu, mallinnus sekä tehokas käyttö
-	Ohjelmoinnin perusteet ja laiteläheinen ohjelmointi
-	Digitaalitekniikan perusteet
-	Big Data ja tiedonlouhinta - perusteet

# LUOTTAMUSTEHTÄVÄT
-	Olen saanut vastuulleni kokonaisen ohjelmiston suunnittelun ja toteutuksen
-	Olen toiminut työpaikallani iltavastaavana ja osastonhoitajana
-	Olen saanut vastuulleni kassojen laskun sekä käteisrahaliikenteen työpaikallani
-	Olen toiminut varakapteenina kahdessa eri jääkiekkojoukkueessa
-	Olen toiminut jääkiekkovalmentajana junioreille yhteensä 6-kautta
-	Toimin armeijassa joukkueenjohtajan viestimiehenä

# KIELITAITO
-	Suomi, äidinkieli
-	Englanti, puhe: hyvä, kirjoitus, hyvä
-	Ruotsi, alkeet

# HARRASTUKSET
-	Jääkiekko, II-divisioona, aloittanut n. 6-vuotiaana
-	Tietokoneet, konsolit, pelaaminen ja muu tietotekniikan räplääminen
